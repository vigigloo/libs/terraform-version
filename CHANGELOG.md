# Changelog


## 0.4.0 (2023-03-20)
### Features
* operator is an option. Exact is only "=". No operator is None.
  ([a08ff78](https://gitlab.com/vigigloo/libs/terraform-version/commit/a08ff783d9f5c255bf248c14ff6715f8d7ca9341))


## 0.3.0 (2023-03-20)
### Features
* VersionRequirement function 'is_exact' ([4bafdcd](https://gitlab.com/vigigloo/libs/terraform-version/commit/4bafdcda0462af0d699c3adc83ba04a1314098be))


## 0.2.0
### Fix
* VersionRequirement: An absence of operator is considered as `=` operator ([baa1bf18](https://gitlab.com/vigigloo/libs/terraform-version/-/commit/baa1bf189abfdb4bfa4818167006dd6f736c7d5c))
* VersionRequirement: `=` can't be combined with other comparators ([ef7e51a1](https://gitlab.com/vigigloo/libs/terraform-version/-/commit/ef7e51a1ba142b1ddfb61f9206b17a8d773a62bd))
### Test
* Tests refactoring
### Docs
* Basic documentation


## 0.1.0
* First version, minimal features available



# terraform-version

[![pipeline status](https://gitlab.com/vigigloo/libs/terraform-version/badges/master/pipeline.svg)](https://gitlab.com/vigigloo/libs/terraform-version/-/commits/main)
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](./LICENSE)
[![Crates.io](https://img.shields.io/crates/v/terraform-version.svg)](https://crates.io/crates/terraform-version)


`terraform-version` is a short parser and match calculator for terraform version constraint syntax.

It follows the [terraform semantic constraints](https://developer.hashicorp.com/terraform/language/expressions/version-constraints).

```toml
[dependencies]
terraform-version = "0.4"
```
*Compiler support: requires rustc 1.67+*


## Example

```rust
use terraform_version::{Version, VersionRequirement, NumericIdentifiers};

fn main() {

    let version_req = VersionRequirement::parse("< 5.4.3, >= 1.2.3").unwrap();

    let version = Version::parse("1.2.3").unwrap();
    assert!(version.matches(&version_req));

    let version = Version::parse("5.4.4").unwrap();
    assert!(!version.matches(&version_req));


    let version_req = VersionRequirement::parse("= 1.2.3-beta").unwrap();

    let version = Version::parse("1.2.3-beta").unwrap();
    assert!(version.matches(&version_req));

    let version = Version {
        numeric_identifiers: NumericIdentifiers::new(vec![1, 2, 3]),
        suffix: None
    };
    assert!(!version.matches(&version_req));

}
```

## License

`terraform-version` is provided under the MIT license. See [LICENSE](./LICENSE).



use crate::Operator;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, thiserror::Error, miette::Diagnostic, PartialEq, Eq)]
pub enum Error {
    /// No version for parsing.
    #[error("No version filled in")]
    NoVersion,

    /// No version requirement for parsing.
    #[error("No version requirement filled in")]
    NoVersionRequirement,

    /// Trying to parse version requirement with suffix whereas `operator` is not `Exact` neither `Different`.
    #[error("Operator `{0}` forbidden. Non-empty suffix is only allowed if operator is Exact of Different")]
    NotAllowedOperatorWithSuffix(Operator),

    /// Trying to parse version requirement with invalid `operator`.
    #[error("Invalid operator in comparator : `{0}`")]
    InvalidOperator(String),

    /// Trying to parse version requirement with multiple comparators whereas one has the operator `Exact`.
    #[error("Impossible to parse version requirement `{0}`. Operator `Exact` is forbidden in multi comparator. Consider removing either the exact operator comparator, or all others.")]
    NotAllowedOperatorWithMultipleComparators(String),

    /// Trying to parse numeric identifier from non number.
    #[error("Impossible to parse numeric identifier `{text}` : `{ni}` is not a number")]
    ImpossibleNumericIdentifierParsing {
        #[source]
        err: std::num::ParseIntError,
        text: String,
        ni: String,
    },
}
